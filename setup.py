"""Installation script for hospital_api application."""
import sys
from os import path

from setuptools import setup

if sys.version_info < (3, 7):
    sys.exit("Sorry, Python < 3.7 is not supported")


DESCRIPTION = "Practice building flask app with REST API."

THIS_DIRECTORY = path.abspath(path.dirname(__file__))
with open(path.join(THIS_DIRECTORY, "README.md"), encoding="utf-8") as f:
    LONG_DESCRIPTION = f.read()

AUTHOR = "Aditya Wibowo"
AUTHOR_EMAIL = "adityawibowo.awp@gmail.com"
PROJECT_URLS = {
    "Documentation": "https://gitlab.com/adityawp/hospital/README.md",
    "Bug Tracker": "https://gitlab.com/adityawp/hospital/-/issues",
    "Source Code": "https://gitlab.com/adityawp/hospital/",
}
INSTALL_REQUIRES = [
    "Flask==2.1.2",
    "werkzeug==2.1.2",
    "psycopg2-binary",
    "Flask-SQLAlchemy",
    "Flask-Migrate",
    "python-dotenv",
    "flask-restx",
    "flask-marshmallow",
    "marshmallow-sqlalchemy",
    "Flask-Cors",
]
EXTRAS_REQUIRE = {
    "dev": [
        "pre-commit",
    ]
}

setup(
    name="hospital_api",
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    version="0.1",
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    maintainer=AUTHOR,
    maintainer_email=AUTHOR_EMAIL,
    license="MIT",
    url="https://gitlab.com/adityawp/hospital",
    project_urls=PROJECT_URLS,
    python_requires=">=3.7",
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
)
