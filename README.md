# Flask Hospital

This web was made for practice creating a flask web with REST API. Made by Aditya Wibowo. this web was made to run on docker.

## Table of Content

- [Flask Hospital](#flask-hospital)
  - [Table of Content](#table-of-content)
  - [Database](#database)
  - [Backend](#backend)
  - [Run the website locally](#run-the-website-locally)

## Database

To build the container for database use this command

```
docker-compose build dba
```

After that use this command to run that container

```
docker-compose up dba
```

To access the database container run this command after the command above is running

```
docker-compose exec dba bash
```
If you want to use the CLI for `postgresql` you can use this command

```
psql -d hospital_api -U warmice
```
The password for the database is `admin54321`

## Backend

To build the container for database use this command

```
docker-compose build backend
```

After that use this command to run that container

```
docker-compose up backend
```

Now the container is running open browser to access it with address
```
http://127.0.0.1:5000/swagger
```
You dont need other app like` postman` to try the API because I use `swagger.ui` the process to use the API can be done via a simple and intuitive UI


To access the backend container run this command after the command above is running

```
docker-compose exec backend bash
```

## Run the website locally

Create a virtual environment for this project, then install all neccessary package with this command
```
pip install -e .[dev]
```
Run this command to run the website
```
flask run --eager-loading
```
currently there is an issue with flask 2.0.1 thats been fixed on 2.0.2 https://github.com/pallets/flask/issues/4096 so you can't use `flask run`

For the database, you need to create the database and change the info on file `app.py` line 18 with your own database.
