FROM python:3.10-slim-bullseye
ENV PYTHONUNBUFFERED 1
WORKDIR /usr/app
COPY . .    
RUN pip install -e .
CMD ["flask", "run", "-h", "0.0.0.0", "-p", "5000"]
