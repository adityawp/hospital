from extensions.ma import ma
from models.doctor import DoctorModel


class DoctorSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = DoctorModel
        load_instance = True
        include_fk = True
