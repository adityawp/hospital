from extensions.ma import ma
from models.patient import PatientModel


class PatientSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = PatientModel
        load_instance = True
        include_fk = True
