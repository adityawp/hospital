from extensions.ma import ma
from models.employee import EmployeeModel


class EmployeeSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = EmployeeModel
        load_instance = True
        include_fk = True
