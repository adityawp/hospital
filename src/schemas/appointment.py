from extensions.ma import ma
from models.appointment import AppointmentModel


class AppointmentSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = AppointmentModel
        load_instance = True
        include_fk = True
