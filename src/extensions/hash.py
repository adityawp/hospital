from werkzeug.security import check_password_hash, generate_password_hash


def generate_hash(password):
    return generate_password_hash(password)


def check_hash(self, password):
    return check_password_hash(self.password, password)
