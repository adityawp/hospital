from sqlalchemy.dialects import postgresql

from extensions.db import db


class AppointmentModel(db.Model):
    __tablename__ = "appointments"

    id = db.Column(db.Integer, primary_key=True)
    patient_id = db.Column(db.Integer, db.ForeignKey("patients.id"), nullable=False)
    doctor_id = db.Column(db.Integer, db.ForeignKey("doctors.id"), nullable=False)
    date_time = db.Column(db.DateTime, nullable=False)
    diagnose = db.Column(db.String(40), nullable=False)
    status = db.Column(
        postgresql.ENUM("IN_QUEUE", "DONE", "CANCELLED", name="status_choice", create_type=False), nullable=False
    )
    diagnose = db.Column(db.Text)
    notes = db.Column(db.Text)

    def __repr__(self):
        return f"Appointment '{self.patient_id}', '{self.doctor_id}', '{self.date_time}'"

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_appointments():
        return AppointmentModel.query.all()

    @staticmethod
    def get_one_appointment(id):
        return AppointmentModel.query.get(id)

    @staticmethod
    def find_doctor_id(value):
        return AppointmentModel.query.filter_by(doctor_id=value).first()

    @staticmethod
    def find_date_time(value):
        return AppointmentModel.query.filter_by(date_time=value).first()
