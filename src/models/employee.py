from extensions.db import db
from extensions.hash import generate_hash


class EmployeeModel(db.Model):
    __tablename__ = "employees"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    gender = db.Column(db.String(10), nullable=False)
    birthdate = db.Column(db.Date, nullable=False)

    def __init__(self, name, username, password, gender, birthdate):
        self.name = name
        self.username = username
        self.password = generate_hash(password)
        self.gender = gender
        self.birthdate = birthdate

    def __repr__(self):
        return f"employee '{self.name}', '{self.username}'"

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_employees():
        return EmployeeModel.query.all()

    @staticmethod
    def get_one_employee(id):
        return EmployeeModel.query.get(id)

    # Use this to grab a username from the model
    @staticmethod
    def find_username(value):
        return EmployeeModel.query.filter_by(username=value).first()
