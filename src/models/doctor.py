from extensions.db import db
from extensions.hash import generate_hash


class DoctorModel(db.Model):
    __tablename__ = "doctors"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    gender = db.Column(db.String(10), nullable=False)
    birthdate = db.Column(db.Date, nullable=False)
    work_start_time = db.Column(db.DateTime, nullable=False)
    work_end_time = db.Column(db.DateTime, nullable=False)

    def __init__(self, name, username, password, gender, birthdate, work_start_time, work_end_time):
        self.name = name
        self.username = username
        self.password = generate_hash(password)
        self.gender = gender
        self.birthdate = birthdate
        self.work_start_time = work_start_time
        self.work_end_time = work_end_time

    def __repr__(self):
        return f"doctor '{self.username}', '{self.work_start_time}', '{self.work_end_time}'"

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_doctors():
        return DoctorModel.query.all()

    @staticmethod
    def get_one_doctor(id):
        return DoctorModel.query.get(id)

    @staticmethod
    def find_username(value):
        return DoctorModel.query.filter_by(username=value).first()
