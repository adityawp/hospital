from extensions.db import db


class PatientModel(db.Model):
    __tablename__ = "patients"

    id = db.Column(db.Integer, primary_key=True)
    no_ktp = db.Column(db.String(18), unique=True, nullable=False)
    name = db.Column(db.String(255), nullable=False)
    gender = db.Column(db.String(10), nullable=False)
    birthdate = db.Column(db.Date, nullable=False)
    address = db.Column(db.String(255), nullable=False)

    def __repr__(self):
        return f"doctor '{self.no_ktp}', '{self.name}'"

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_patients():
        return PatientModel.query.all()

    @staticmethod
    def get_one_patient(id):
        return PatientModel.query.get(id)

    # Use this to grab a no_ktp from the model
    @staticmethod
    def find_no_ktp(value):
        return PatientModel.query.filter_by(no_ktp=value).first()
