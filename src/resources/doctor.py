from flask import request
from flask_restx import Namespace, Resource, fields, inputs, reqparse
from werkzeug.security import generate_password_hash

from models.doctor import DoctorModel
from schemas.doctor import DoctorSchema

DOCTOR_NOT_FOUND = "Doctor not found."

doctor_ns = Namespace("doctor", description="doctor related operations")
doctors_ns = Namespace("doctors", description="doctors related operations")

doctor_schema = DoctorSchema()
doctor_list_schema = DoctorSchema(many=True)

doctor = doctors_ns.model(
    "Doctor",
    {
        "name": fields.String,
        "username": fields.String,
        "password": fields.String,
        "gender": fields.String,
        "birthdate": fields.Date,
        "work_start_time": fields.DateTime,
        "work_end_time": fields.DateTime,
    },
)


class Doctor(Resource):
    def get(self, id):
        doctor_data = DoctorModel.get_one_doctor(id)
        if doctor_data:
            return doctor_schema.dump(doctor_data)
        return {"message": DOCTOR_NOT_FOUND}, 404

    def delete(self, id):
        doctor_data = DoctorModel.get_one_doctor(id)
        if doctor_data:
            doctor_data.delete()
            return {"message": "Doctor Deleted successfully"}, 200
        return {"message": DOCTOR_NOT_FOUND}, 404

    def put(self, id):
        doctor_json = request.get_json()
        doctor_data = DoctorModel.get_one_doctor(id)

        if doctor_data:
            doctor_data.name = doctor_json["name"]
            doctor_data.username = doctor_json["username"]
            doctor_data.password = generate_password_hash(doctor_json["password"])
            doctor_data.gender = doctor_json["gender"]
            doctor_data.birthdate = doctor_json["birthdate"]
            doctor_data.work_start_time = doctor_json["work_start_time"]
            doctor_data.work_end_time = doctor_json["work_end_time"]
        else:
            doctor_data = doctor_schema.load(doctor_json)

        doctor_data.save()
        return doctor_schema.dump(doctor_data), 200


class DoctorList(Resource):
    @doctors_ns.doc("Get all the Doctors")
    def get(self):
        return doctor_list_schema.dump(DoctorModel.get_all_doctors()), 200

    @doctors_ns.doc("Create a Doctor")
    def post(self):
        doctor_json = request.get_json()
        doctor_data = doctor_schema.load(doctor_json)

        doctor_data.save()

        return doctor_schema.dump(doctor_data), 201
