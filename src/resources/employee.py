from flask import request
from flask_restx import Namespace, Resource, fields, inputs, reqparse
from werkzeug.security import generate_password_hash

from models.employee import EmployeeModel
from schemas.employee import EmployeeSchema

EMPLOYEE_NOT_FOUND = "Employee not found."

employee_ns = Namespace("employee", description="employee related operations")
employees_ns = Namespace("employees", description="employees related operations")

employee_schema = EmployeeSchema()
employee_list_schema = EmployeeSchema(many=True)

employee = employees_ns.model(
    "Employee",
    {
        "name": fields.String,
        "username": fields.String,
        "password": fields.String,
        "gender": fields.String,
        "birthdate": fields.Date,
    },
)


class Employee(Resource):
    def get(self, id):
        employee_data = EmployeeModel.get_one_employee(id)
        if employee_data:
            return employee_schema.dump(employee_data)
        return {"message": EMPLOYEE_NOT_FOUND}, 404

    def delete(self, id):
        employee_data = EmployeeModel.get_one_employee(id)
        if employee_data:
            employee_data.delete()
            return {"message": "Employee Deleted successfully"}, 200
        return {"message": EMPLOYEE_NOT_FOUND}, 404

    def put(self, id):
        employee_json = request.get_json()
        employee_data = EmployeeModel.get_one_employee(id)

        if employee_data:
            employee_data.name = employee_json["name"]
            employee_data.username = employee_json["username"]
            employee_data.password = generate_password_hash(employee_json["password"])
            employee_data.gender = employee_json["gender"]
            employee_data.birthdate = employee_json["birthdate"]
        else:
            employee_data = employee_schema.load(employee_json)

        employee_data.save()
        return employee_schema.dump(employee_data), 200


class EmployeeList(Resource):
    @employees_ns.doc("Get all the Employees")
    def get(self):
        return employee_list_schema.dump(EmployeeModel.get_all_employees()), 200

    @employees_ns.doc("Create an Employee")
    def post(self):
        employee_json = request.get_json()
        employee_data = employee_schema.load(employee_json)

        employee_data.save()

        return employee_schema.dump(employee_data), 201
