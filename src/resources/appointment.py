from flask import request
from flask_restx import Namespace, Resource, fields, reqparse

from models.appointment import AppointmentModel
from schemas.appointment import AppointmentSchema

APPOINTMENT_NOT_FOUND = "Appointment not found."

appointment_ns = Namespace("appointment", description="appointment related operations")
appointments_ns = Namespace("appointments", description="appointments related operations")

appointment_schema = AppointmentSchema()
appointment_list_schema = AppointmentSchema(many=True)

appointment = appointments_ns.model(
    "Appointment",
    {
        "patient_id": fields.Integer,
        "doctor_id": fields.Integer,
        "date_time": fields.DateTime,
        "status": fields.String,
        "diagnose": fields.String,
        "notes": fields.String,
    },
)


class Appointment(Resource):
    def get(self, id):
        appointment_data = AppointmentModel.get_one_appointment(id)
        if appointment_data:
            return appointment_schema.dump(appointment_data)
        return {"message": APPOINTMENT_NOT_FOUND}, 404

    def delete(self, id):
        appointment_data = AppointmentModel.get_one_appointment(id)
        if appointment_data:
            appointment_data.delete()
            return {"message": "Appointment Deleted successfully"}, 200
        return {"message": APPOINTMENT_NOT_FOUND}, 404

    def put(self, id):
        appointment_json = request.get_json()
        appointment_data = AppointmentModel.get_one_appointment(id)

        if appointment_data:
            appointment_data.patient_id = appointment_json["patient_id"]
            appointment_data.doctor_id = appointment_json["doctor_id"]
            appointment_data.date_time = appointment_json["date_time"]
            appointment_data.status = appointment_json["status"]
            appointment_data.diagnose = appointment_json["diagnose"]
            appointment_data.notes = appointment_json["notes"]
        else:
            appointment_data = appointment_schema.load(appointment_json)

        appointment_data.save()
        return appointment_schema.dump(appointment_data), 200


class AppointmentList(Resource):
    @appointments_ns.doc("Get all the Appointments")
    def get(self):
        return appointment_list_schema.dump(AppointmentModel.get_all_appointments()), 200

    @appointments_ns.doc("Create an Appointment")
    def post(self):
        appointment_json = request.get_json()
        appointment_data = appointment_schema.load(appointment_json)

        if (
            AppointmentModel.find_doctor_id(appointment_json["doctor_id"])
            and AppointmentModel.find_date_time(appointment_json["date_time"])
            and appointment_json["status"] == "IN_QUEUE"
        ):
            return {"message": "The doctor already has an appointment at that time."}, 400

        appointment_data.save()

        return appointment_schema.dump(appointment_data), 201
