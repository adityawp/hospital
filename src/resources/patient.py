import os

from flask import request
from flask_restx import Namespace, Resource, fields, inputs, reqparse

from models.patient import PatientModel
from schemas.patient import PatientSchema

PATIENT_NOT_FOUND = "Patient not found."

patient_ns = Namespace("patient", description="patient related operations")
patients_ns = Namespace("patients", description="patients related operations")

patient_schema = PatientSchema()
patient_list_schema = PatientSchema(many=True)

patient = patients_ns.model(
    "Patient",
    {
        "no_ktp": fields.String,
        "name": fields.String,
        "gender": fields.String,
        "birthdate": fields.Date,
        "address": fields.String,
    },
)


class Patient(Resource):
    def get(self, id):
        patient_data = PatientModel.get_one_patient(id)
        if patient_data:
            return patient_schema.dump(patient_data)
        return {"message": PATIENT_NOT_FOUND}, 404

    def delete(self, id):
        patient_data = PatientModel.get_one_patient(id)
        if patient_data:
            patient_data.delete()
            return {"message": "Patient Deleted successfully"}, 200
        return {"message": PATIENT_NOT_FOUND}, 404

    def put(self, id):
        patient_json = request.get_json()
        patient_data = PatientModel.get_one_patient(id)

        if patient_data:
            patient_data.no_ktp = patient_json["no_ktp"]
            patient_data.name = patient_json["name"]
            patient_data.gender = patient_json["gender"]
            patient_data.birthdate = patient_json["birthdate"]
            patient_data.address = patient_json["address"]
        else:
            patient_data = patient_schema.load(patient_json)

        patient_data.save()
        return patient_schema.dump(patient_data), 200


class PatientList(Resource):
    @patients_ns.doc("Get all the Patients")
    def get(self):
        return patient_list_schema.dump(PatientModel.get_all_patients()), 200

    @patients_ns.doc("Create a Patient")
    def post(self):
        patient_json = request.get_json()
        patient_data = patient_schema.load(patient_json)

        patient_data.save()

        return patient_schema.dump(patient_data), 201
