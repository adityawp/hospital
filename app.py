import os

from flask import Blueprint, Flask
from flask_migrate import Migrate
from flask_restx import Api

from src.extensions.db import db
from src.extensions.ma import ma
from src.resources.appointment import (
    Appointment,
    AppointmentList,
    appointment_ns,
    appointments_ns,
)
from src.resources.doctor import Doctor, DoctorList, doctor_ns, doctors_ns
from src.resources.employee import Employee, EmployeeList, employee_ns, employees_ns
from src.resources.patient import Patient, PatientList, patient_ns, patients_ns

app = Flask(__name__)
api = Api(app, doc="/swagger", title="Hospital API")

app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["PROPAGATE_EXCEPTIONS"] = True
migrate = Migrate(app, db)

api.add_namespace(doctor_ns)
api.add_namespace(doctors_ns)
api.add_namespace(employee_ns)
api.add_namespace(employees_ns)
api.add_namespace(patient_ns)
api.add_namespace(patients_ns)
api.add_namespace(appointment_ns)
api.add_namespace(appointments_ns)

db.init_app(app)
ma.init_app(app)

doctor_ns.add_resource(Doctor, "/<int:id>")
doctors_ns.add_resource(DoctorList, "")
employee_ns.add_resource(Employee, "/<int:id>")
employees_ns.add_resource(EmployeeList, "")
patient_ns.add_resource(Patient, "/<int:id>")
patients_ns.add_resource(PatientList, "")
appointment_ns.add_resource(Appointment, "/<int:id>")
appointments_ns.add_resource(AppointmentList, "")

if __name__ == "__main__":
    app.run()
